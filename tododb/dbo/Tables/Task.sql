﻿CREATE TABLE [dbo].[Task] (
    [Id]              BIGINT           IDENTITY (1, 1) NOT NULL,
    [ListId]          UNIQUEIDENTIFIER NOT NULL,
    [Name]            NVARCHAR (255)   NULL,
    [Description]     NVARCHAR (MAX)   NULL,
    [Status]          INT              NULL,
    [Completion Date] DATETIME         NULL,
    [Created Date]    DATETIME         DEFAULT (getdate()) NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_ListTask] FOREIGN KEY ([ListId]) REFERENCES [dbo].[List] ([Id]) ON DELETE CASCADE
);
GO
CREATE NONCLUSTERED INDEX IX_dbo_Task ON dbo.[Task]([Id], [ListId], [Name], [Description], [Status], [Completion Date], [Created Date]) 
GO
