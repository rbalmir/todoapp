#TLS 2.0
[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

$baseURL = "https://localhost:44372/api"

# List OPERATIONS
# ===============

#Get all Lists
$get_list_all = "$baseURL/list/all"
$list_all = Invoke-WebRequest -Uri $url_list_all -Method Get -ContentType "application/json" -UseDefaultCredentials | ConvertFrom-Json 

#Create a List
$post_list = "$baseURL/list"
$list_01 = Invoke-WebRequest -Uri $post_list -Method Post -ContentType "application/json" -UseDefaultCredentials | ConvertFrom-Json 

#Read a List
$listid = $list_01.listInfo.listId.ToString()
$read_list = "$baseURL/list/$listid"
$list_02 = Invoke-WebRequest -Uri $read_list -Method Get -ContentType "application/json" -UseDefaultCredentials | ConvertFrom-Json 

#??? Not working
#Put - updated list name 
$listid = $list_01.listInfo.listId.ToString()
$put_list = "$baseURL/list/$listid"
$body = @{ name="Tester 1" } | ConvertTo-Json
$list_03 = Invoke-RestMethod -Uri $put_list -Method Put -ContentType "application/json" -UseDefaultCredentials -Body $body | ConvertFrom-Json 
#$list_03 = Invoke-RestMethod -Uri $put_list -Method Put -ContentType "application/json" -UseDefaultCredentials | ConvertFrom-Json 

#Delete list name 
$listid = "3b924a8d-5ae0-4233-a668-63c8b2feb900"
$delete_list = "$baseURL/list/$listid"
$list_04 = Invoke-RestMethod -Uri $put_list -Method DELETE -ContentType "application/json" -UseDefaultCredentials | ConvertFrom-Json 

# TASK OPERATIONS
# ===============
#Config a List
$post_list = "$baseURL/list"
$list_05 = Invoke-WebRequest -Uri $post_list -Method Post -ContentType "application/json" -UseDefaultCredentials | ConvertFrom-Json

$listid = $list_05.listInfo.listId.ToString()
$put_list = "$baseURL/list/$listid"
$body = @{ name="Task Test List 01" } | ConvertTo-Json
$list_03 = Invoke-RestMethod -Uri $put_list -Method Put -ContentType "application/json" -UseDefaultCredentials -Body $body | ConvertFrom-Json 

#Create a Task
$post_task = "$baseURL/list/$listid/task"
$task_01 = Invoke-WebRequest -Uri $post_task -Method Post -ContentType "application/json" -UseDefaultCredentials | ConvertFrom-Json

#Put - Update task
$bodyTaskdata = @{ 
    name="Tester 1"
    status=1
    description="This is a new description"
} | ConvertTo-Json

$taskid = $task_01.taskinfo.taskId
$put_task = "$baseURL/list/$listid/task/$taskid"
$task_02 = Invoke-WebRequest -Uri $put_task -Method Put -ContentType "application/json" -UseDefaultCredentials -Body $bodyTaskdata | ConvertFrom-Json

#Read - Task
$get_task = "$baseURL/list/$listid/task/$taskid"
$task_03 = Invoke-WebRequest -Uri $get_task -Method GET -ContentType "application/json" -UseDefaultCredentials | ConvertFrom-Json

#Delete - Task
$delete_task = "$baseURL/list/$listid/task/$taskid"
$task_04 = Invoke-WebRequest -Uri $delete_task -Method DELETE -ContentType "application/json" -UseDefaultCredentials | ConvertFrom-Json

