﻿<#
.SYNOPSIS
Build Database ORM

Install EF tools
Install-Package Microsoft.EntityFrameworkCore.Tools

Dependent Packages (In order to use EF)
Microsoft.EntityFrameworkCore
Microsoft.EntityFrameworkCore.Design
Microsoft.EntityFrameworkCore.SqlServer

Must run this script within the package manager console.
#>

$projectName = "dataorm"

$tables = 
'dbo.List',
'dbo.Task'

Scaffold-DbContext "Server=localhost\SQLEXPRESS;Database=tododb;User Id=sa;Password=111111;" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Models -Tables $tables -Project $projectName -Force

