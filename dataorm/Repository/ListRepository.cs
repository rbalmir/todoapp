﻿using System;
using System.Collections.Generic;
using System.Text;
using data.RepoInterfaces;
using dataorm.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using data.RepoInterfaces.Models;

namespace dataorm.Repository
{

    public static class ListExtentions
    {
        public static ListData ToAList(this List list)
        {
            return new ListData()
            {
                ListId = list?.Id.ToString(),
                Name = list?.Name
            };
        }
    }

    public class ListRepository : IListRepository
    {
        private tododbContext db = null;

        public ListRepository(tododbContext db)
        { this.db = db; }


        public void DeleteList(string listId)
        {
            var alist = this.db.List.Where(_ => new Guid(listId) == _.Id).FirstOrDefault();

            if(alist != null)
            {
                this.db.List.Remove(alist);
                this.db.SaveChanges();            
            }
            else
                throw new Exception($"ListId:{listId} does not exist. Delete failed");

        }

        public ListData GetList(string listId)
        {

            var list = this.db.List.AsNoTracking()
                .Where(_ => new Guid(listId) == _.Id).FirstOrDefault();

            return list?.ToAList();
        }

        public IEnumerable<ListData> GetLists()
        {
            var lists = this.db.List.AsNoTracking().Select(_ => _.ToAList());

            return lists;
        }

        //Create empty
        public ListData InsertList(ListData inlist)
        {
            //ID is expected to be null on insert/record creation.
            if (inlist.ListId != null)
                return null;

            //ID is populated by the database on new records.
            var list = new List()
            {
                Name = inlist.Name
            };

            this.db.List.Add(list);
            this.db.SaveChanges();

            return list.ToAList();
        }

        public void UpdateList(ListData inlist)
        {
            string listId = inlist.ListId;
            string name = inlist.Name;

            var list = this.db.List.Where(_ => _.Id.ToString() == listId).FirstOrDefault();

            if (list != null)
            {
                list.Name = name;
                this.db.SaveChanges();
            }
            else
            {
                throw new Exception($"ListId:{listId} does not exist. Update failed");
            }

        }

    }

}
