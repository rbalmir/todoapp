﻿using System;
using System.Collections.Generic;
using System.Text;
using data.RepoInterfaces;
using dataorm.Models;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using data.RepoInterfaces.Models;

namespace dataorm.Repository
{

    public static class TaskExtentions
    {
        public static TaskData ToATask(this Task t)
        {
            return new TaskData()
            {
                ListId = t?.Id.ToString(),
                TaskId = t.Id,
                Name = t?.Name,
                Description = t?.Description,
                Status = t.Status.Value,
                CompletionDate = t?.CompletionDate,
                CreatedDate = t.CreatedDate
            };
        }
    }

    public class TaskRepository : ITaskRepository
    {
        private tododbContext db = null;

        public TaskRepository(tododbContext db)
        { this.db = db; }

        public void DeleteTask(string listId, int taskId)
        {
            var t = this.db.Task.Where(_ => new Guid(listId) == _.ListId && _.Id == taskId).FirstOrDefault();

            if (t != null)
                this.db.Task.Remove(t);
            else
                throw new Exception($"List:{listId} TaskId:{taskId} doesn't exist");

            this.db.SaveChanges();            
        }

        public IEnumerable<TaskData> GetTasks(string listId)
        {
            var t = this.db.Task.AsNoTracking()
                .Where(_ => new Guid(listId) == _.ListId)
                .Select(_ => _.ToATask());

            return t;
        }

        public TaskData GetTask(string listId, int taskId)
        {
            var t = this.db.Task.AsNoTracking()
                            .Where(_ => _.ListId.ToString() == listId && _.Id == taskId)
                            .FirstOrDefault();

            return t?.ToATask();
        }

        //Create empty
        public TaskData InsertTask(TaskData inTask)
        {
            //ID is expected to be null/0 on insert/record creation.
            if (inTask.TaskId != 0)
                return null;
          
            //ID is populated by the database on new records.
            var task = new Task()
            {
                Name = inTask.Name,
                ListId = new Guid(inTask.ListId),
                Description = inTask.Description,
                CompletionDate = inTask.CompletionDate,
                Status = 0 //inTask.Status              
            };

            this.db.Task.Add(task);
            this.db.SaveChanges();

            return task.ToATask();
        }
        /*
        public void UpdateList(ListData inlist)
        {
            string listId = inlist.ListId;
            string name = inlist.Name;

            var list = this.db.List.Where(_ => _.Id.ToString() == listId).FirstOrDefault();

            if (list != null)
                list.Name = name;

        }
        */

        public void UpdateTask(TaskData taskChanges)
        {
            //
            string listId = taskChanges.ListId;
            long taskId = taskChanges.TaskId;

            var existingtask = this.db.Task.Where(_ => _.ListId.ToString() == listId && _.Id == taskId).FirstOrDefault();

            if (existingtask != null)
            {
                existingtask.Name = taskChanges.Name;
                existingtask.Description = taskChanges.Description;
                existingtask.Status = taskChanges.Status;
                existingtask.CompletionDate = taskChanges.CompletionDate;

                this.db.SaveChanges();
            }
            else
                throw new Exception($"ListId:{listId} TaskId:{taskId} does not exist. Update failed");

        }

    }

}
