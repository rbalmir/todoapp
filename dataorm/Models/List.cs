﻿using System;
using System.Collections.Generic;

namespace dataorm.Models
{
    public partial class List
    {
        public List()
        {
            Task = new HashSet<Task>();
        }

        public Guid Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Task> Task { get; set; }
    }
}
