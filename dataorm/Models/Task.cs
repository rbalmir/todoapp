﻿using System;
using System.Collections.Generic;

namespace dataorm.Models
{
    public partial class Task
    {
        public int Id { get; set; }
        public Guid ListId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int? Status { get; set; }
        public DateTime? CompletionDate { get; set; }
        public DateTime CreatedDate { get; set; }

        public virtual List List { get; set; }
    }
}
