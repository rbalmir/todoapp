﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace todo.Models
{
    public class ListInfo
    {
        public string ListId { get; set; }
        public string Name { get; set; }
    }

    public class AList
    {
        public ListInfo ListInfo { get; set; }

        public List<TaskInfo> Tasks { get; set; }
    }


    public class TaskInfo
    {
        
        public int TaskId { get; set; }

        public string Name { get; set; }

        //Pending|Completed
        [Range(0, 1)]
        public int Status { get; set; }

        public DateTime? CompletionDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Description { get; set; }
    }

    public class ATask
    {
        public string ListId { get; set; }

        public TaskInfo TaskInfo { get; set; }

    }

}
