﻿using System.Collections.Generic;
using System.Linq;
using todo.Models;
using data.RepoInterfaces.Models;

namespace todo.Models
{
    public static class ListAPIExtensions
    {
        public static ListInfo ToListInfo(this ListData alist)
        {
            ListInfo listInfo = new ListInfo()
            {
                ListId = alist.ListId,
                Name = alist.Name
            };

            return listInfo;
        }

        public static TaskInfo ToTaskInfo(this TaskData atask)
        {
            TaskInfo taskInfo = new TaskInfo()
            {
                TaskId = atask.TaskId,
                Description = atask.Description,
                CompletionDate = atask.CompletionDate,
                CreatedDate = atask.CreatedDate,
                Status = atask.Status.Value,
                Name = atask.Name            
            };

            return taskInfo;
        }

        public static TaskData ToTaskData(this TaskInfo atask, string listId)
        {
            TaskData taskInfo = new TaskData()
            {
                TaskId = atask.TaskId,
                Description = atask.Description,
                CompletionDate = atask.CompletionDate,
                CreatedDate = atask.CreatedDate,
                Status = atask.Status,
                Name = atask.Name,
                ListId = listId
            };

            return taskInfo;
        }


        public static List<TaskInfo> ToTaskInfo(this List<TaskData> atasks)
        {
            return atasks.Select(d => d.ToTaskInfo()).ToList();
        }


    }
}
