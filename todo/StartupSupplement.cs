﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore.SqlServer;
using Microsoft.EntityFrameworkCore;
using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using data.RepoInterfaces;
using data.RepoInterfaces.Models;
using dataorm.Models;
using dataorm.Repository;

namespace todo
{
    public class StartupSupplement
    {
        public static void ConfigureAdditionalServices(IServiceCollection services, IConfiguration configuration)
        {
            string csDB = configuration.GetConnectionString("todoDB");

            //TODO: Maybe throw a specific exception.
            if (csDB == null)
                throw new Exception("todoDB Connection String Is Null");

            services.AddDbContext<tododbContext>(options => options.UseSqlServer(csDB), ServiceLifetime.Scoped);

            services.AddScoped<IListRepository, ListRepository>();
            services.AddScoped<ITaskRepository, TaskRepository>();
        }

    }
}
