﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using todo.Models;
using data.RepoInterfaces;
using data.RepoInterfaces.Models;

namespace todo.Controllers.API
{

    [Route("api/list")]
    [ApiController]
    public class ListApiController : ControllerBase
    {
        IListRepository listRepo;
        ITaskRepository taskRepo;

        public ListApiController(IListRepository listRepo, ITaskRepository taskRepo)
        {
            this.listRepo = listRepo;
            this.taskRepo = taskRepo;
        }

        // GET: api/list/all
        [HttpGet("all")]
        public IEnumerable<ListInfo> GetLists()
        {
            List<ListInfo> listInfoData = listRepo.GetLists()
                .Select(d => d.ToListInfo())
                .ToList();

            this.HttpContext.Response.StatusCode = StatusCodes.Status200OK;

            return listInfoData;
        }

        //POST: api/list        
        [HttpPost]
        public AList CreateList()
        {            
            ListData createdList = listRepo.InsertList(new ListData() 
            { Name = "" }
            );

            AList newList = new AList()
            {
                ListInfo = createdList.ToListInfo(),
                Tasks = new List<TaskInfo>()
            };

            this.HttpContext.Response.StatusCode = StatusCodes.Status200OK;

            return newList;
        }

        // GET: api/ListApi/5
        [HttpGet("{listid}")]
        public AList ReadList([FromRoute]string listId)
        {
            ListData listData = listRepo.GetList(listId);
            List<TaskData> tasksData = taskRepo.GetTasks(listId).ToList();

            if(listData != null)
            {
                AList alist = new AList()
                {
                    ListInfo = listData.ToListInfo(),
                    Tasks = tasksData.ToTaskInfo()
                };
                
                this.HttpContext.Response.StatusCode = StatusCodes.Status200OK;
                return alist;            
            }
            else
            {
                this.HttpContext.Response.StatusCode = StatusCodes.Status404NotFound;
            }

            return null;
        }

        public class NameContainer
        {
            public string Name { get; set; }
        }

        [HttpPut("{listid}")]
        public void UpdateListName([FromRoute]string listId, [FromBody] NameContainer nameContainer)
        {
            string name = nameContainer.Name;
            try
            {
                ListData ld = new ListData()
                {
                    ListId = listId,
                    Name = name
                };

                listRepo.UpdateList(ld);

                this.HttpContext.Response.StatusCode = StatusCodes.Status200OK;
            }
            catch
            {
                this.HttpContext.Response.StatusCode = StatusCodes.Status404NotFound;
            }

        }

        [HttpDelete("{listid}")]
        public void DeleteList([FromRoute]string listId)
        {
            try
            {
                listRepo.DeleteList(listId);
                this.HttpContext.Response.StatusCode = StatusCodes.Status200OK;
            }
            catch
            {
                this.HttpContext.Response.StatusCode = StatusCodes.Status404NotFound;
            }

        }


// Tasks
//-------------------

// POST: api/list/?/task
        [HttpPost("{listid}/task")]
        public ATask CreateTask(string listId)
        {
            //List must exist before creating a task
            if (listRepo.GetList(listId) != null)
            {

                TaskData createdTask = taskRepo.InsertTask(new TaskData() {
                    ListId = listId,
                    Name = "New Task"
                });

                ATask newTask = new ATask()
                {
                    ListId = createdTask.ListId,
                    TaskInfo = createdTask.ToTaskInfo()
                };

                this.HttpContext.Response.StatusCode = StatusCodes.Status200OK;

                return newTask;
            }
            else
            {
                this.HttpContext.Response.StatusCode = StatusCodes.Status404NotFound;
            }

            return null;
        }

        [HttpGet("{listid}/task/{taskid}")]
        public ATask ReadTask([FromRoute]string listId, [FromRoute]int taskId)
        {
            //List must exist before creating a task
            if (listRepo.GetList(listId) != null)
            {
                TaskData existingTaskdata = taskRepo.GetTask(listId, taskId);

                ATask existingTask = new ATask()
                {
                    ListId = existingTaskdata.ListId,
                    TaskInfo = existingTaskdata.ToTaskInfo()
                };

                this.HttpContext.Response.StatusCode = StatusCodes.Status200OK;

                return existingTask;
            }
            else
            {
                this.HttpContext.Response.StatusCode = StatusCodes.Status404NotFound;
            }

            return null;
        }

        //UPDATES
        #region UPDATETASK

        [HttpPut("{listid}/task/{taskid}")]
        public void UpdateTask([FromRoute]string listId, [FromRoute]int taskId, [FromBody] TaskInfo taskInfo)
        {
            try
            {
                TaskData taskDataChanges = taskInfo.ToTaskData(listId);
                taskDataChanges.TaskId = taskId;

                TaskData taskDataExisiting = taskRepo.GetTask(listId, taskId);

                if(taskDataExisiting.Status == 0 && taskDataChanges.Status == 1)
                    taskDataChanges.CompletionDate = DateTime.Now;

                //Remove completed date if status returns back to pending.
                if (taskDataExisiting.Status == 1 && taskDataChanges.Status == 0)
                    taskDataChanges.CompletionDate = null;


                taskRepo.UpdateTask(taskDataChanges);
                this.HttpContext.Response.StatusCode = StatusCodes.Status200OK;
            }
            catch
            {
                this.HttpContext.Response.StatusCode = StatusCodes.Status404NotFound;
            }

        }
        /*
        [HttpPut("{listid}/task/{taskid}/name")]
        public AList UpdateTaskName([FromRoute]int listId, [FromRoute]int taskId, [FromBody] string name)
        {
            return null;
        }

        [HttpPut("{listid}/task/{taskid}/description")]
        public AList UpdateTaskDescription([FromRoute]int listId, [FromRoute]int taskId, [FromBody] string description)
        {
            return null;
        }

        [HttpPut("{listid}/task/{taskid}/status")]
        public AList UpdateTaskStatus([FromRoute]int listId, [FromRoute]int taskId, [FromBody] int status)
        {
            //Valid onlyh 0|1
            return null;
        }
        */

        #endregion

        [HttpDelete("{listid}/task/{taskid}")]
        public void DeleteTask([FromRoute]string listId, [FromRoute]int taskId)
        {

            try
            {                
                taskRepo.DeleteTask(listId, taskId);
                this.HttpContext.Response.StatusCode = StatusCodes.Status200OK;
            }
            catch
            {
                this.HttpContext.Response.StatusCode = StatusCodes.Status404NotFound;
            }

        }
    }
}
