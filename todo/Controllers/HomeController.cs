﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using data.RepoInterfaces;
using data.RepoInterfaces.Models;
using Microsoft.AspNetCore.Mvc;
using todo.Models;

namespace todo.Controllers
{
    public class HomeController : Controller
    {
        IListRepository listRepo;
        ITaskRepository taskRepo;

        public HomeController(IListRepository listRepo, ITaskRepository taskRepo)
        {
            this.listRepo = listRepo;
            this.taskRepo = taskRepo;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet("{listId}")]
        public IActionResult Index(string listId)
        {
            var idContainer = new ActiveListViewModel() { ListId = listId };

            return View(idContainer);
        }

        public IActionResult NewList()
        {            
            ListData created = listRepo.InsertList(new ListData() { Name = "New List" });
            string id = created.ListId;

            return RedirectToAction("Index", new { listId = id });
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
