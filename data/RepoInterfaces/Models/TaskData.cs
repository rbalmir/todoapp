﻿using System;

namespace data.RepoInterfaces.Models
{
    public class TaskData
    {
        public string ListId { get; set; }
        public int TaskId { get; set; }

        public string Name { get; set; }
        public string Description { get; set; }

        public int? Status { get; set; }
        public DateTime? CompletionDate { get; set; }
        public DateTime CreatedDate { get; set; }
    }

}
