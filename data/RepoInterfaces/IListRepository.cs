﻿using System.Collections.Generic;
using System.Text;
using data.RepoInterfaces.Models;

namespace data.RepoInterfaces
{

    public interface IListRepository
    {
        IEnumerable<ListData> GetLists();

        ListData GetList(string listId);

        ListData InsertList(ListData list);
        void UpdateList(ListData list);
        void DeleteList(string listId);
    }

}
