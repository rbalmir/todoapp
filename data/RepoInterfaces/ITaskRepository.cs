﻿using System.Collections.Generic;
using data.RepoInterfaces.Models;

namespace data.RepoInterfaces
{
    public interface ITaskRepository
    {
        IEnumerable<TaskData> GetTasks(string listId);

        TaskData GetTask(string listId, int taskId);
        TaskData InsertTask(TaskData task);
        void UpdateTask(TaskData task);
        void DeleteTask(string listId, int taskId);
    }

}
